Bluetooth
=========

For full documentation visit `bluez.org <http://www.bluez.org>`_.

Commands
--------

Start and Discover nearby devices
---------------------------------

::

    root@am335x-vcu:~# bluetoothctl
    [NEW] Controller 0C:B2:B7:11:61:9B am335x-vcu [default]
    [NEW] Device 40:4E:36:55:DE:CE niconico

    [bluetooth]# power on
    Changing power on succeeded
    [CHG] Controller 0C:B2:B7:11:61:9B Powered: yes

    [bluetooth]# agent on
    Agent registered

    [bluetooth]# default-agent
    Default agent request successful

    [bluetooth]# discoverable on
    [CHG] Controller 0C:B2:B7:11:61:9B Class: 0x200000
    Changing discoverable on succeeded
    [CHG] Controller 0C:B2:B7:11:61:9B Discoverable: yes

    [bluetooth]# scan on
    Discovery started
    [CHG] Controller 0C:B2:B7:11:61:9B Discovering: yes
    [NEW] Device 56:D2:37:FA:DC:8B 56-D2-37-FA-DC-8B
    [NEW] Device 74:8D:3C:66:C9:7D 74-8D-3C-66-C9-7D
    [NEW] Device 5A:D3:22:54:BD:6C 5A-D3-22-54-BD-6C
    [NEW] Device CC:5B:4F:F4:8A:2B fenix 3 HR
    [CHG] Device 74:8D:3C:66:C9:7D RSSI: -80
    [NEW] Device 6F:90:CD:32:DE:1B 6F-90-CD-32-DE-1B
    [NEW] Device 46:FB:E1:BC:5F:C8 46-FB-E1-BC-5F-C8
    [CHG] Device 6F:90:CD:32:DE:1B RSSI: -103
    [CHG] Device 6F:90:CD:32:DE:1B RSSI: -85
    [CHG] Device 5A:D3:22:54:BD:6C RSSI: -75
    [CHG] Device 74:8D:3C:66:C9:7D RSSI: -82
    [CHG] Device 40:4E:36:55:DE:CE RSSI: -52
    [CHG] Device 5A:D3:22:54:BD:6C RSSI: -66

    [bluetooth]# scan off
    [CHG] Device E0:E5:CF:96:FA:09 RSSI is nil
    [CHG] Device 40:4E:36:55:DE:CE RSSI is nil
    [CHG] Device 46:FB:E1:BC:5F:C8 RSSI is nil

    [CHG] Controller 0C:B2:B7:11:61:9B Discovering: no
    Discovery stopped
