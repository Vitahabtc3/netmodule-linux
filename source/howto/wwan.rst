.. _wwan:

WWAN
====

Overview
--------

Supported modems and firmware (Caution: older versions may not work stable or miss features!)

- u-blox TOBY-L210, firmware 17.00,A01.02
- u-blox LARA-L6, firmware 03.15,A00.01

Preparation
-----------

1. Insert SIM Card in sim slot

Usage
-----

All the configuration can be done by NetworkManager but sometimes it can be
useful to check lower level configurations with ModemManager.

Firmware update
~~~~~~~~~~~~~~~

.. warning:: Downgrading a WWAN modem will likely lead to a broken modem

On some devices, the modem is delivered with an old firmware, reported by `list-devices`
as "obsolete" or "deprecated". It is highly recommended to upgrade it.


How to check firmare version and upgrade is explain at :ref:`fw-update`.


Initial configuration
~~~~~~~~~~~~~~~~~~~~~

The WWAN modem is configured at each boot by a script named wwan-config.
This script is using the configuration file `/etc/wwan/wwan0.conf` to setup
the modem before letting ModemManager handle it.

This configuration file is divided in three sections:
 * apn: must configured to use a private APN
 * sim: is used to choose between the different sim cards available on the device
 * ublox: low level configurations for ublox modem. This should normaly not be modified

APN configuration
#################

.. note:: This section only applies on a device with a u-blox TOBY-L2 modem.
   for other modems, it will be ignored.

When using a private APN, this section has to be configured with the following fields:
::

    [apn]
    apn=<APN>
    user=<USER>
    password=<PASSWORD>

When the default APN provided by the network when using LTE must be used, make sure that this fields are not set.

After any change to this file, the system has to rebooted or the following command to be run:

.. code-block:: console

    $ systemctl restart wwan-config@wwan0

SIM card configuration
######################

This section is used to choose which SIM card to use with the modem.
There are four SIM cards slot that can be used by the modem.

.. code-block:: none

    [sim]
    SIM=<value>

Where <value> can be :

* auto: The script will detect if a physical SIM card is present and switch to m2m SIM card (soldered to the board) if it is not the case
* sim1: Use the physical SIM card on the main board
* m2m: Use the m2m SIM card soldered on the main board
* ui-top: Use the SIM card that is on top of the User Interface
* ui-btm: Use the SIM card that is on the bottom of the User Interface


After any change to this file, the system has to be rebooted or the following command to be run:

.. code-block:: console

    $ systemctl restart wwan-config@wwan0



NetworkManager commands
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

    $ # Create connection
    $ nmcli c add type gsm con-name wwan ifname "" ipv6.method ignore gsm.apn <APN>

    $ # Create connection with APN authentication
    $ nmcli c add type gsm con-name wwan ifname "" ipv6.method ignore \
      gsm.apn <APN> gsm.username <USER> gsm.password <PASSWORD>

    $ # Set PIN number
    $ nmcli c modify wwan gsm.pin <pin number>

    $ # Start the connection
    $ nmcli c up wwan

ModemManager configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

    $ mmcli -L  # list modems and get modem id
    $ mmcli -M  # list modems in a loop, useful when waiting after a reset
    $ mmcli -m 0 # See state of the modem 0
    $ mmcli -i 0 --pin=<pin number>  # Entering pin on modem 0
    $ mmcli -m 0 -r # Reset the modem



Low level configuration / modem debugging
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It can sometimes be useful to send directly some commands to the modem.
The preferred way to do this is to ask wwan-config to stop all modem operations
and then use a tool like minicom to communicate directly with the modem as in
the following commands:

.. note:: ModemManager is monitored by wwan-config so it cannot be stopped without
   wwan-config restarting it. Stopping wwan-config on the other hand will also power
   off the modem.

.. code-block:: console

    # USR1 asks wwan-config to stop modem operations
    kill -s USR1 $(</run/wwan0.pid)

    # Open minicom on AT interface
    minicom -D /dev/wwan0
       AT+CCID # This may not show up if echo is not enabled (ATE1)
       +CCID: 894101182777XYXYXYXY

       OK
    # Exit minicom with Ctrl+A Q

    # USR2 asks wwan-config to restart modem operations
    kill -s USR2 $(</run/wwan0.pid)


ModemManager extensions
~~~~~~~~~~~~~~~~~~~~~~~

The ModemManager version used in NetModule linux is the version 1.14.X
with some NetModule specific extensions.

It is maintained and up to date with latest bug and security fixes.

NetModule did the following changes to the community version :

1. Support configuration of default EPS bearer for u-blox toby-l2 modems
########################################################################

   In 4G (LTE), the handling of the APN configuration is different than
   in 2G and 3G and specific to each vendor. Toby-l2 modems is not supported
   in the community version.


2. Handling of reconnect requests
#################################

   Events like reconnection and disconnection on the radio side trigger
   AT messages that are not handled by ModemManager. The NetModule
   version handles this messages, leading to a faster reconnection.


3. Change AT commands timeout to 3 minutes
##########################################

   The u-blox modems can take up to 3 minutes before answering and AT
   command. The default timeout varies between 3 and 60 seconds depending
   on the commands. This difference made MM send more commands while the modem was
   still processing the first one, leading to a lock of the modem.

4. Show more precise signal quality in output of mmcli -m
###########################################################

   By default MM is showing a pretty coarse value for signal quality when
   showing modem status with `mmcli -m 0` (20% steps). With this change
   MM is showing a more precise value, more representative of what is retrieved
   with `mmcli -m 0 --signal-get`.

eUICC
-----

Support for eUICC (Embedded Universal Integrated Circuit Card) is dropped as of v1.6.5, due to product management decision.

