# File: source/requirements.txt

# Defining the exact version will make sure things don't break
sphinx==3.4.3
docutils < 0.17
sphinx_rtd_theme==1.0.0
readthedocs-sphinx-search==0.1.0rc3
