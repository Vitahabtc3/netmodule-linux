Release Notes
=============

OEM Linux 1.6.5 / 24-Mar-2023
----------------------------------------------------------

Added features

- kernel maintenance: update to v5.10.176
- wwan: compatibility with latest toby-l2 firmware: v17.00,A01.02
   - IMPORTANT NOTE: Please update all toby-l2 modems to the latest firmware!

Removed features

- eUICC support

Fixed Bugs

- #364178 iperf in revers udp reboots device
- #365665 am335x shutdown is hanging forever

OEM Linux 1.6.4 / 10-Mar-2023
----------------------------------------------------------

Fixed Bugs

- #373444 VCUpro: RS232/RS485 selector pin misbehavior
- #373448 VCUpro: RTS gets activated as soon as the port is closed (RS232 mode)
- #375849 OEM Linux fails to build minimal image


OEM Linux 1.6.3 / 17-Feb-2023
----------------------------------------------------------

Fixed Bugs


- #366277 fct: hw25: can't remove names of unused GPIOs
- #366279 fct: hw24: pps supprt for timepulse test missing
- #366281 fct: hw24: sensors configuration lacks ignition-voltage
- #366284 fct: hw25: sensors configuration

OEM Linux 1.6.2 / 6-Feb-2023
----------------------------------------------------------

Added features


- kernel maintenance: update to v5.10.166

Fixed Bugs


- #359191 OEM Linux: Release Pipeline wrongly fails with "Tag already exists"
- #359204 OEM Linux: Release Pipeline corrupts srcrev entries in recipes
- #359226 U-Boot Repository Redundancy: some recipes were referencing wrong internal repo
- #359227 Release Pipeline is failing while syncing legacy repos
- #359229 OEM Linux: Release Pipeline did not send a slack notification after repo sync stage failed
- #359376 wwan-config is crashing with LARA-L6 when a default APN is configured
- #359419 Aurix Update Failure: not reproducible

OEM Linux 1.6.1 / 23-Jan-2023
----------------------------------------------------------

Added features


- OEM Linux: Modem Lara-L6 Support
    - kernel: enable lara-l6 detection
    - modemmanager: ublox lara-l6 support
- OEM Linux: neo-m9 support
    - neo-m9: nmubxlib update
    - neo-m9: gnss-mgr update
- kernel maintenance: update to v5.10.164 and v4.14.303

Known Issues


- #359376 wwan-config is crashing with LARA-L6 when a default APN is configured

OEM Linux 1.6.0 / 28-Nov-2022
----------------------------------------------------------

Added features


- OEM Linux: eUICC support
- Added support for NG800, board v3.2 and new machine based on this board (hw26-vcupro)
- kernel maintenance: kernel: update to v5.10.149 and v4.14.295

Fixed Bugs


- [81329] kernel: can: backported mainline implementation for 64 bits messages objects
  to replace custom NetModule implementation which had a bug


OEM Linux 1.5.2 / 30-Jun-2022
----------------------------------------------------------

Added
~~~~~

- Nothing

Changed
~~~~~~~

-  [80015] yocto: Remove all dependencies to MACHINE variable when
   building the kernel, allowing better reusability of the kernel for
   different hardware sharing the same kernel
-  [80088] yocto: using a shared sstate mirror
-  [80137] wwan-config: Small code readability improvements
-  [80391] linux: Updated with latest security patches to 5.10.126
-  [79481] HW17: integrated in our OEM Linux yocto environment
-  [80072] OEM Linux: get rid of release revisions include file (set in
   recipes)
-  [79425] yocto: Updated community layers with latest security patches
-  [79996] linux: Updated with latest security patches to 5.10.120 and
   4.14.282
-  [79987] yocto: Reorganization of the kernel recipes to split
   different versions of the kernel

Fixed
~~~~~

-  [80178] wwan-config: Fixed rare cases where the modem stayed poweroff
   forever
-  [80331] yocto: Fixed deployment of netmodule-fitimage when building
   entirely from sstate

Known Issues
~~~~~~~~~~~~
-  [77282] hw23: dhcp bootarg is not supported
